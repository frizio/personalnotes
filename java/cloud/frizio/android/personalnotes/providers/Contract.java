package cloud.frizio.android.personalnotes.providers;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;

/**
 * Created by frizio on 7/12/16.
 */
public class Contract {

    public static final String CONTENT_AUTHORITY = "cloud.frizio.android.personalnotes.mynotesprovider";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);


    public static final class Notes {

        public static final String TABLE_NAME = "notes";

        // public static final String COLUMN_ID = TABLE_NAME + "_id";
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_TEXT = TABLE_NAME + "_text";
        public static final String COLUMN_DATE = TABLE_NAME + "_date";


        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();


        public static Uri buildUri(long id){

            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildUriFilter(String filter){

            return CONTENT_URI.buildUpon().appendPath(filter).build();
        }


        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TABLE_NAME;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TABLE_NAME;



        public static final String[] ALL_COLUMNS = {COLUMN_ID, COLUMN_TEXT, COLUMN_DATE};

    }

}
