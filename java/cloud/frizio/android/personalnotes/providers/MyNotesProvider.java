package cloud.frizio.android.personalnotes.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by frizio on 7/15/16.
 */
public class MyNotesProvider extends ContentProvider {

    private Db db;

    private static final String TAG = "MyNotesProvider";

    private static final int CODE_NOTES = 100;
    private static final int CODE_NOTE = 101;


    private static final UriMatcher uriMatcher = buildUriMatcher();

    static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        final String authority = Contract.CONTENT_AUTHORITY;

        matcher.addURI(authority, Contract.Notes.TABLE_NAME, CODE_NOTES);

        matcher.addURI(authority, Contract.Notes.TABLE_NAME + "/#", CODE_NOTE);

        return matcher;
    }


    @Override
    public boolean onCreate() {
        db = new Db(getContext());

        return true;
    }


    @Nullable
    @Override
    public String getType(Uri uri) {

        String type = null;

        switch (uriMatcher.match(uri)) {
            case CODE_NOTES:
                type = Contract.Notes.CONTENT_TYPE;
                break;
            case CODE_NOTE:
                type = Contract.Notes.CONTENT_ITEM_TYPE;
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        String msg = "getType: " + uri.toString() + ", " + type;
        Log.d(TAG, msg);

        return type;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor cursor = null;

        switch (uriMatcher.match(uri)) {
            case CODE_NOTES:
                cursor = db.getReadableDatabase().query(
                        Contract.Notes.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null, null, sortOrder
                );
                break;
            case CODE_NOTE:
                cursor = db.getReadableDatabase().query(
                            Contract.Notes.TABLE_NAME,
                            projection,
                            Contract.Notes.COLUMN_ID + " = ?",
                            new String[]{uri.getLastPathSegment()},
                            null, null, sortOrder
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        String msg = "getType: " + uri.toString() + ", " + cursor.getCount();
        Log.d(TAG, msg);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        Uri returnUri = null;
        long id = -1;

        switch (uriMatcher.match(uri)) {
            case CODE_NOTES:
                id = db.getWritableDatabase().insert(
                        Contract.Notes.TABLE_NAME,
                        null,
                        values
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        returnUri = ContentUris.withAppendedId(uri, id);

        String msg = "insert: " + uri.toString() + ", " + returnUri.toString();
        Log.d(TAG, msg);

        if(!returnUri.getLastPathSegment().equals("-1"))
            getContext().getContentResolver().notifyChange(uri, null);

        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        int rowsDeleted = 0;

        switch (uriMatcher.match(uri)) {
            case CODE_NOTES:
                rowsDeleted = db.getWritableDatabase().delete(
                                 Contract.Notes.TABLE_NAME,
                                 selection,
                                 selectionArgs
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if(rowsDeleted > 0)
            getContext().getContentResolver().notifyChange(uri, null);

        String msg = "delete: " + uri.toString() + ", " + rowsDeleted;
        Log.d(TAG, msg);

        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        int rowsUpdated = 0;

        switch (uriMatcher.match(uri)) {
            case CODE_NOTE:
                rowsUpdated = db.getWritableDatabase().update(
                                 Contract.Notes.TABLE_NAME,
                                 values,
                                 Contract.Notes.COLUMN_ID + " = ?",
                                 new String[]{uri.getLastPathSegment()}
                );
                break;
            case CODE_NOTES:
                rowsUpdated = db.getWritableDatabase().update(
                                 Contract.Notes.TABLE_NAME,
                                 values,
                                 selection,
                                 selectionArgs
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }


        if(rowsUpdated > 0)
            getContext().getContentResolver().notifyChange(uri, null);

        String msg = "update: " + uri.toString() + ", " + rowsUpdated;
        Log.d(TAG, msg);

        return rowsUpdated;
    }

    /*
    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        return super.bulkInsert(uri, values);
    }
    */
}
