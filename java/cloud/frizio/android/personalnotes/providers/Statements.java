package cloud.frizio.android.personalnotes.providers;

/**
 * Created by frizio on 7/15/16.
 */
public class Statements {

    private static final String START_STATEMENTS = "CREATE TABLE IF NOT EXISTS ";

    public static final String NOTES_CREATE_STATEMENT =
            START_STATEMENTS +
            Contract.Notes.TABLE_NAME + " (" +
            Contract.Notes.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + ", " +
            Contract.Notes.COLUMN_TEXT + " TEXT" + ", " +
            Contract.Notes.COLUMN_DATE + " TEXT default CURRENT_TIMESTAMP" + ");";

}
