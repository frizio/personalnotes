package cloud.frizio.android.personalnotes.providers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by frizio on 7/15/16.
 */
public class Db extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "notes.db";

    private static final int DATABASE_VERSION = 1;


    public Db(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(Statements.NOTES_CREATE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
