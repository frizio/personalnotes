package cloud.frizio.android.personalnotes;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import cloud.frizio.android.personalnotes.providers.Contract;

/**
 * Created by frizio on 7/14/16.
 */
public class NotesCursorAdapter extends CursorAdapter {


    public NotesCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return LayoutInflater.from(context).inflate(
                                                R.layout.note_list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        String noteText = cursor.getString(
                cursor.getColumnIndex(Contract.Notes.COLUMN_TEXT)
        );

        int pos = noteText.indexOf(10); // 10 is ASCII code for \n.
        if (pos != -1) {
            noteText = noteText.substring(0, pos) + " ...";
        }

        TextView textView = (TextView) view.findViewById(R.id.textViewNote);
        textView.setText(noteText);

    }
}
